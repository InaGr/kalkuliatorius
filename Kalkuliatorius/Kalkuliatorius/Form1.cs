﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalkuliatorius
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //private void Vienas_Click(object sender, EventArgs e) ARBA RASYTI SITAIP.
        //{
        //    if (Ekranas.Text.Length >= 8)
        //    {
        //        return;
        //    }
        //    Ekranas.Text = Ekranas.Text + "1";
        //}

        private string x;
        private string operacija;

        private void Vienas_Click(object sender, EventArgs e)
        {
            skaiciai("1");
        }
        private void Du_Click(object sender, EventArgs e)
        {
            skaiciai("2");
        }

        private void Tris_Click(object sender, EventArgs e)
        {
            skaiciai("3");
        }

        private void Keturi_Click(object sender, EventArgs e)
        {
            skaiciai("4");
        }

        private void Penki_Click(object sender, EventArgs e)
        {
            skaiciai("5");
        }

        private void Sesi_Click(object sender, EventArgs e)
        {
            skaiciai("6");
        }

        private void Septyni_Click(object sender, EventArgs e)
        {
            skaiciai("7");
        }

        private void Astuoni_Click(object sender, EventArgs e)
        {
            skaiciai("8");
        }

        private void Devyni_Click(object sender, EventArgs e)
        {
            skaiciai("9");
        }

        private void Nulis_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text != "0")
            {
                skaiciai("0");
            }
        }

        // sukurem f-ja, kuri naudojama kiekvienam mygtukui
        public void skaiciai (string skaicius)
        {
            if (Ekranas.Text.Length < 8)
            {
                Ekranas.Text = Ekranas.Text + skaicius;
            }
            else
            {
                MessageBox.Show("Jus paspaudete daugiau negu galimas skaicius");
            }            
        }


        private void Suma_Click(object sender, EventArgs e)
        {
            x = Ekranas.Text;
            Ekranas.Clear(); //Ekranas.Text = "";
            operacija = "+";
        }

        private void Rezultatas_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text == "") // GERIAUSIA PRIES PRADEDANT KAZKA DARYTI -  VALIDUOTI
            {
                return;
            }

            double X = Convert.ToDouble(x);
            double result = 0;
            double Y = Convert.ToDouble(Ekranas.Text);

            //if (Ekranas.Text == "") // ARBA
            //{
            //     Y = 0;
            //}
            //else
            //{
            //    Y = Convert.ToDouble(Ekranas.Text);
            //}

            //if (Ekranas.Text == "") //ARBA PARASYTI TAIP
            //{
            //    return;
            //}


            if (operacija == "+")
            {
                result = X + Y;
            }
            else if (operacija == "-")
            {
                result = X - Y;
            }
            else if (operacija == "/")
            {
                result = X / Y;
            }
            else if (operacija == "*")
            {
                result = X * Y;
            }

            Ekranas.Text = result.ToString();
        }

        private void Skirtumas_Click(object sender, EventArgs e)
        {
            x = Ekranas.Text;
            Ekranas.Clear();
            operacija = "-";
        }

        private void Dalyba_Click(object sender, EventArgs e)
        {
            x = Ekranas.Text;
            Ekranas.Clear();
            operacija = "/";
        }

        private void Daugyba_Click(object sender, EventArgs e)
        {
            x = Ekranas.Text;
            Ekranas.Clear();
            operacija = "*";
        }

        private void PliusMInus_Click(object sender, EventArgs e)
        {
            x = Ekranas.Text;
            if(Ekranas.Text != "")
            {
                double X = Convert.ToDouble(x);
                Ekranas.Text = (X * (-1)).ToString();
            }
        }

        private void Taskas_Click(object sender, EventArgs e)
        {
            if(!Ekranas.Text.Contains("."))
            {
                if (Ekranas.Text == "")
                {
                    Ekranas.Text = Ekranas.Text + "0";
                }
                Ekranas.Text = Ekranas.Text + ".";
            }
        }

        private void C_Click(object sender, EventArgs e)
        {
            Ekranas.Clear();
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            if(Ekranas.Text != "")
            {
                Ekranas.Text = Ekranas.Text.Remove(Ekranas.Text.Length - 1);
            }
        }

        private void Saknys_Click(object sender, EventArgs e)
        {
            x = Ekranas.Text;
            double X = Convert.ToDouble(x);
            Ekranas.Text = Math.Sqrt(X).ToString();
        }
    }
}
