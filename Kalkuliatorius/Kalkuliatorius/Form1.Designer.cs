﻿namespace Kalkuliatorius
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Ekranas = new System.Windows.Forms.TextBox();
            this.CE = new System.Windows.Forms.Button();
            this.C = new System.Windows.Forms.Button();
            this.Delete = new System.Windows.Forms.Button();
            this.Dalyba = new System.Windows.Forms.Button();
            this.Septyni = new System.Windows.Forms.Button();
            this.Keturi = new System.Windows.Forms.Button();
            this.Vienas = new System.Windows.Forms.Button();
            this.PliusMInus = new System.Windows.Forms.Button();
            this.Nulis = new System.Windows.Forms.Button();
            this.Du = new System.Windows.Forms.Button();
            this.Penki = new System.Windows.Forms.Button();
            this.Astuoni = new System.Windows.Forms.Button();
            this.Taskas = new System.Windows.Forms.Button();
            this.Tris = new System.Windows.Forms.Button();
            this.Sesi = new System.Windows.Forms.Button();
            this.Devyni = new System.Windows.Forms.Button();
            this.Rezultatas = new System.Windows.Forms.Button();
            this.Suma = new System.Windows.Forms.Button();
            this.Skirtumas = new System.Windows.Forms.Button();
            this.Daugyba = new System.Windows.Forms.Button();
            this.Saknys = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Ekranas
            // 
            this.Ekranas.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ekranas.Location = new System.Drawing.Point(44, 42);
            this.Ekranas.Name = "Ekranas";
            this.Ekranas.Size = new System.Drawing.Size(436, 53);
            this.Ekranas.TabIndex = 0;
            this.Ekranas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // CE
            // 
            this.CE.Location = new System.Drawing.Point(76, 101);
            this.CE.Name = "CE";
            this.CE.Size = new System.Drawing.Size(69, 57);
            this.CE.TabIndex = 1;
            this.CE.Text = "CE";
            this.CE.UseVisualStyleBackColor = true;
            // 
            // C
            // 
            this.C.Location = new System.Drawing.Point(151, 101);
            this.C.Name = "C";
            this.C.Size = new System.Drawing.Size(69, 57);
            this.C.TabIndex = 2;
            this.C.Text = "C";
            this.C.UseVisualStyleBackColor = true;
            this.C.Click += new System.EventHandler(this.C_Click);
            // 
            // Delete
            // 
            this.Delete.Location = new System.Drawing.Point(226, 101);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(69, 57);
            this.Delete.TabIndex = 3;
            this.Delete.Text = "DEL";
            this.Delete.UseVisualStyleBackColor = true;
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // Dalyba
            // 
            this.Dalyba.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dalyba.Location = new System.Drawing.Point(301, 101);
            this.Dalyba.Name = "Dalyba";
            this.Dalyba.Size = new System.Drawing.Size(69, 57);
            this.Dalyba.TabIndex = 4;
            this.Dalyba.Text = "÷";
            this.Dalyba.UseVisualStyleBackColor = true;
            this.Dalyba.Click += new System.EventHandler(this.Dalyba_Click);
            // 
            // Septyni
            // 
            this.Septyni.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F);
            this.Septyni.Location = new System.Drawing.Point(76, 164);
            this.Septyni.Name = "Septyni";
            this.Septyni.Size = new System.Drawing.Size(69, 57);
            this.Septyni.TabIndex = 5;
            this.Septyni.Text = "7";
            this.Septyni.UseVisualStyleBackColor = true;
            this.Septyni.Click += new System.EventHandler(this.Septyni_Click);
            // 
            // Keturi
            // 
            this.Keturi.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F);
            this.Keturi.Location = new System.Drawing.Point(76, 227);
            this.Keturi.Name = "Keturi";
            this.Keturi.Size = new System.Drawing.Size(69, 57);
            this.Keturi.TabIndex = 6;
            this.Keturi.Text = "4";
            this.Keturi.UseVisualStyleBackColor = true;
            this.Keturi.Click += new System.EventHandler(this.Keturi_Click);
            // 
            // Vienas
            // 
            this.Vienas.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Vienas.Location = new System.Drawing.Point(76, 290);
            this.Vienas.Name = "Vienas";
            this.Vienas.Size = new System.Drawing.Size(69, 57);
            this.Vienas.TabIndex = 7;
            this.Vienas.Text = "1";
            this.Vienas.UseVisualStyleBackColor = true;
            this.Vienas.Click += new System.EventHandler(this.Vienas_Click);
            // 
            // PliusMInus
            // 
            this.PliusMInus.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PliusMInus.Location = new System.Drawing.Point(76, 353);
            this.PliusMInus.Name = "PliusMInus";
            this.PliusMInus.Size = new System.Drawing.Size(69, 57);
            this.PliusMInus.TabIndex = 8;
            this.PliusMInus.Text = "±";
            this.PliusMInus.UseVisualStyleBackColor = true;
            this.PliusMInus.Click += new System.EventHandler(this.PliusMInus_Click);
            // 
            // Nulis
            // 
            this.Nulis.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nulis.Location = new System.Drawing.Point(151, 353);
            this.Nulis.Name = "Nulis";
            this.Nulis.Size = new System.Drawing.Size(69, 57);
            this.Nulis.TabIndex = 12;
            this.Nulis.Text = "0";
            this.Nulis.UseVisualStyleBackColor = true;
            this.Nulis.Click += new System.EventHandler(this.Nulis_Click);
            // 
            // Du
            // 
            this.Du.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Du.Location = new System.Drawing.Point(151, 290);
            this.Du.Name = "Du";
            this.Du.Size = new System.Drawing.Size(69, 57);
            this.Du.TabIndex = 11;
            this.Du.Text = "2";
            this.Du.UseVisualStyleBackColor = true;
            this.Du.Click += new System.EventHandler(this.Du_Click);
            // 
            // Penki
            // 
            this.Penki.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F);
            this.Penki.Location = new System.Drawing.Point(151, 227);
            this.Penki.Name = "Penki";
            this.Penki.Size = new System.Drawing.Size(69, 57);
            this.Penki.TabIndex = 10;
            this.Penki.Text = "5";
            this.Penki.UseVisualStyleBackColor = true;
            this.Penki.Click += new System.EventHandler(this.Penki_Click);
            // 
            // Astuoni
            // 
            this.Astuoni.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F);
            this.Astuoni.Location = new System.Drawing.Point(151, 164);
            this.Astuoni.Name = "Astuoni";
            this.Astuoni.Size = new System.Drawing.Size(69, 57);
            this.Astuoni.TabIndex = 9;
            this.Astuoni.Text = "8";
            this.Astuoni.UseVisualStyleBackColor = true;
            this.Astuoni.Click += new System.EventHandler(this.Astuoni_Click);
            // 
            // Taskas
            // 
            this.Taskas.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Taskas.Location = new System.Drawing.Point(226, 353);
            this.Taskas.Name = "Taskas";
            this.Taskas.Size = new System.Drawing.Size(69, 57);
            this.Taskas.TabIndex = 16;
            this.Taskas.Text = ".";
            this.Taskas.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Taskas.UseVisualStyleBackColor = true;
            this.Taskas.Click += new System.EventHandler(this.Taskas_Click);
            // 
            // Tris
            // 
            this.Tris.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F);
            this.Tris.Location = new System.Drawing.Point(226, 290);
            this.Tris.Name = "Tris";
            this.Tris.Size = new System.Drawing.Size(69, 57);
            this.Tris.TabIndex = 15;
            this.Tris.Text = "3";
            this.Tris.UseVisualStyleBackColor = true;
            this.Tris.Click += new System.EventHandler(this.Tris_Click);
            // 
            // Sesi
            // 
            this.Sesi.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F);
            this.Sesi.Location = new System.Drawing.Point(226, 227);
            this.Sesi.Name = "Sesi";
            this.Sesi.Size = new System.Drawing.Size(69, 57);
            this.Sesi.TabIndex = 14;
            this.Sesi.Text = "6";
            this.Sesi.UseVisualStyleBackColor = true;
            this.Sesi.Click += new System.EventHandler(this.Sesi_Click);
            // 
            // Devyni
            // 
            this.Devyni.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F);
            this.Devyni.Location = new System.Drawing.Point(226, 164);
            this.Devyni.Name = "Devyni";
            this.Devyni.Size = new System.Drawing.Size(69, 57);
            this.Devyni.TabIndex = 13;
            this.Devyni.Text = "9";
            this.Devyni.UseVisualStyleBackColor = true;
            this.Devyni.Click += new System.EventHandler(this.Devyni_Click);
            // 
            // Rezultatas
            // 
            this.Rezultatas.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rezultatas.Location = new System.Drawing.Point(301, 353);
            this.Rezultatas.Name = "Rezultatas";
            this.Rezultatas.Size = new System.Drawing.Size(69, 57);
            this.Rezultatas.TabIndex = 20;
            this.Rezultatas.Text = "=";
            this.Rezultatas.UseVisualStyleBackColor = true;
            this.Rezultatas.Click += new System.EventHandler(this.Rezultatas_Click);
            // 
            // Suma
            // 
            this.Suma.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Suma.Location = new System.Drawing.Point(301, 290);
            this.Suma.Name = "Suma";
            this.Suma.Size = new System.Drawing.Size(69, 57);
            this.Suma.TabIndex = 19;
            this.Suma.Text = "+";
            this.Suma.UseVisualStyleBackColor = true;
            this.Suma.Click += new System.EventHandler(this.Suma_Click);
            // 
            // Skirtumas
            // 
            this.Skirtumas.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Skirtumas.Location = new System.Drawing.Point(301, 227);
            this.Skirtumas.Name = "Skirtumas";
            this.Skirtumas.Size = new System.Drawing.Size(69, 57);
            this.Skirtumas.TabIndex = 18;
            this.Skirtumas.Text = "-";
            this.Skirtumas.UseVisualStyleBackColor = true;
            this.Skirtumas.Click += new System.EventHandler(this.Skirtumas_Click);
            // 
            // Daugyba
            // 
            this.Daugyba.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Daugyba.Location = new System.Drawing.Point(301, 164);
            this.Daugyba.Name = "Daugyba";
            this.Daugyba.Size = new System.Drawing.Size(69, 57);
            this.Daugyba.TabIndex = 17;
            this.Daugyba.Text = "x";
            this.Daugyba.UseVisualStyleBackColor = true;
            this.Daugyba.Click += new System.EventHandler(this.Daugyba_Click);
            // 
            // Saknys
            // 
            this.Saknys.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Saknys.Location = new System.Drawing.Point(372, 101);
            this.Saknys.Name = "Saknys";
            this.Saknys.Size = new System.Drawing.Size(69, 57);
            this.Saknys.TabIndex = 21;
            this.Saknys.Text = "√";
            this.Saknys.UseVisualStyleBackColor = true;
            this.Saknys.Click += new System.EventHandler(this.Saknys_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(535, 447);
            this.Controls.Add(this.Saknys);
            this.Controls.Add(this.Rezultatas);
            this.Controls.Add(this.Suma);
            this.Controls.Add(this.Skirtumas);
            this.Controls.Add(this.Daugyba);
            this.Controls.Add(this.Taskas);
            this.Controls.Add(this.Tris);
            this.Controls.Add(this.Sesi);
            this.Controls.Add(this.Devyni);
            this.Controls.Add(this.Nulis);
            this.Controls.Add(this.Du);
            this.Controls.Add(this.Penki);
            this.Controls.Add(this.Astuoni);
            this.Controls.Add(this.PliusMInus);
            this.Controls.Add(this.Vienas);
            this.Controls.Add(this.Keturi);
            this.Controls.Add(this.Septyni);
            this.Controls.Add(this.Dalyba);
            this.Controls.Add(this.Delete);
            this.Controls.Add(this.C);
            this.Controls.Add(this.CE);
            this.Controls.Add(this.Ekranas);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Ekranas;
        private System.Windows.Forms.Button CE;
        private System.Windows.Forms.Button C;
        private System.Windows.Forms.Button Delete;
        private System.Windows.Forms.Button Dalyba;
        private System.Windows.Forms.Button Septyni;
        private System.Windows.Forms.Button Keturi;
        private System.Windows.Forms.Button Vienas;
        private System.Windows.Forms.Button PliusMInus;
        private System.Windows.Forms.Button Nulis;
        private System.Windows.Forms.Button Du;
        private System.Windows.Forms.Button Penki;
        private System.Windows.Forms.Button Astuoni;
        private System.Windows.Forms.Button Taskas;
        private System.Windows.Forms.Button Tris;
        private System.Windows.Forms.Button Sesi;
        private System.Windows.Forms.Button Devyni;
        private System.Windows.Forms.Button Rezultatas;
        private System.Windows.Forms.Button Suma;
        private System.Windows.Forms.Button Skirtumas;
        private System.Windows.Forms.Button Daugyba;
        private System.Windows.Forms.Button Saknys;
    }
}

